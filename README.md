## 介绍
MnsClient 客户端
## Install & Use

Add require to your `composer.json`
```json
{
  "require": {
     "jufubao/mnsclient": "~1.0"
  }
}
```
Use Composer to install requires
```bash
composer require jufubao/mnsclient
```

Publish config
```
php bin/hyperf.php vendor:publish jufubao/mnsclient
```