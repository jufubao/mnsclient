<?php

return [
    'end_point' => env('MNS_END_POINT', ''),
    // 请求超时
    'request_timeout' => 8,
    // 消息队列前缀
    'queue_name_prefix' => env('MNS_QUEUE_NAME_PREFIX', ''),
    // Command 消息队列名
    'command_queue_name' => ''
];