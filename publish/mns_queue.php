<?php

declare(strict_types=1);


return [
    'default' => [
        'queue_name' => 'xxx',  // 消息名称
        'wait_seconds' => 5, // 接收消息时等待时长
        'processes' => 4, // 进程数
        'max_messages' => 2000 // 最大处理消息数
    ],
];
