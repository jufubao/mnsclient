<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace MnsClient;


class ConfigProvider
{
    public function __invoke(): array
    {
        return [
            'commands' => [
            ],
            'publish' => [
                [
                    'id' => 'config/mns',
                    'description' => 'The config for mns.',
                    'source' => __DIR__ . '/../publish/mns.php',
                    'destination' => BASE_PATH . '/config/autoload/mns.php',
                ],
                [
                    'id' => 'config/aliyun',
                    'description' => 'The config for aliyun.',
                    'source' => __DIR__ . '/../publish/aliyun.php',
                    'destination' => BASE_PATH . '/config/autoload/aliyun.php',
                ],
                [
                    'id' => 'config/aliyun',
                    'description' => 'The config for aliyun.',
                    'source' => __DIR__ . '/../publish/mns_queue.php',
                    'destination' => BASE_PATH . '/config/autoload/mns_queue.php',
                ],
            ]
        ];
    }
}
