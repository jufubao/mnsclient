<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace MnsClient;

class Result
{
    /**
     * Acknowledge the message.
     */
    public const ACK = 'ack';

    /**
     * Reject the message and requeue it.
     */
    public const REQUEUE = 'requeue';

    /*
    * Unacknowledged the message.
    */
    public const NACK = 'nack';

    /**
     * Acknowledge the message.
     */
    public static function ack(): array {
        return [self::ACK];
    }

    /**
     * Reject the message and requeue it.
     * @param int $visibilityTimeout 下次显示时间
     */
    public static function requeue(int $visibilityTimeout): array {
        return [self::REQUEUE, $visibilityTimeout];
    }

    /**
     * Unacknowledged the message.
     */
    public static function nack(): array {
        return [self::NACK];
    }
}