<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace MnsClient;

use AliyunMNS\Client;
use AliyunMNS\Config;
use Hyperf\Contract\ConfigInterface;

class MnsFactory
{
    public function __construct(protected ConfigInterface $config)
    {
    }

    public function get(): MnsClient
    {
        $aliyunConfig = $this->config->get('aliyun');
        $mnsConfig = $this->config->get('mns');

        $config = new Config();
        $config->setRequestTimeout($mnsConfig['request_timeout']); // 请求超时时间

        $client = new Client($mnsConfig['end_point'], $aliyunConfig['access_key_id'], $aliyunConfig['access_key_secret'],NULL, $config);
        return new MnsClient($client, $mnsConfig);
    }
}